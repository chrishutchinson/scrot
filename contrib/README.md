The contrib directory contains contributions for
Scrot.

Desktop "convenience" entries
- scrot-area -- Capture Area shell script
- scrot-delayed -- Capture entire Desktop after a 20 second delay shell script
- scrot-full -- Capture Full screen shell script
- scrot.area.desktop -- Capture Area desktop entry
- scrot.delayed.desktop -- Capture Delayed decktop entry
- scrot.desktop -- Capture Full desktop entry
- scrot.png -- Scrot Icon for the above (and scrot)
